# Configure the Microsoft Azure Provider.
terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = ">= 2.26"
    }
  }
}

provider "azurerm" {
  features {}
}

# Create a resource group
resource "azurerm_resource_group" "rg" {
  name     = "${var.prefix}RG"
  location = var.location
  tags     = var.tags
}

# Create virtual network
resource "azurerm_virtual_network" "vnet0" {
  name                = "${var.prefix}Vnet"
  address_space       = ["10.0.0.0/16"]
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  tags                = var.tags
}

# Create subnet
resource "azurerm_subnet" "subnet0" {
  name                 = "${var.prefix}Subnet0"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet0.name
  address_prefixes       = ["10.0.1.0/24"]
}

# Create public IP
resource "azurerm_public_ip" "publicip0" {
  name                = "${var.prefix}PublicIP0"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Dynamic"
  tags                = var.tags
}

# Create subnet
resource "azurerm_subnet" "subnet1" {
  name                 = "${var.prefix}Subnet1"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet0.name
  address_prefixes       = ["10.0.2.0/24"]
}

# Create public IP
resource "azurerm_public_ip" "publicip1" {
  name                = "${var.prefix}PublicIP1"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method   = "Dynamic"
  tags                = var.tags
}


# Create network interface
resource "azurerm_network_interface" "nic0" {
  name                = "${var.prefix}NIC0"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  tags                = var.tags

  ip_configuration {
    name                          = "${var.prefix}NICConfg0"
    subnet_id                     = azurerm_subnet.subnet0.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.publicip0.id
  }
}


# Create network interface
resource "azurerm_network_interface" "nic1" {
  name                = "${var.prefix}NIC1"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  tags                = var.tags

  ip_configuration {
    name                          = "${var.prefix}NICConfg1"
    subnet_id                     = azurerm_subnet.subnet1.id
    private_ip_address_allocation = "dynamic"
    public_ip_address_id          = azurerm_public_ip.publicip1.id
  }
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "nsg_linux" {
  name                = "${var.prefix}NSG0"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  tags                = var.tags

  security_rule {
    name                       = "SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "Allow-Ping"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Icmp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  } 
  
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "nsg_win" {
  name                = "${var.prefix}NSG1"
  location            = var.location
  resource_group_name = azurerm_resource_group.rg.name
  tags                = var.tags

  security_rule {
    name                       = "Window-Rule"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "*"
    source_port_range          = "*"
    destination_port_ranges    = [5985, 22, 3389]
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  security_rule {
    name                       = "Allow-Ping"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Icmp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }  
}

# resource "azurerm_network_security_group" "newNSG" {
#   name                = "vm0nsg0"
#   location            = "southeastasia"
#   resource_group_name = azurerm_resource_group.rg.name
#   security_rule {
#     access                     = "Allow"
#     description                = "SSH inbound rule"
#     destination_address_prefix = "*"
#     destination_port_range     = "22"
#     direction                  = "Inbound"
#     name                       = "SSH"
#     priority                   = 1001
#     protocol                   = "Tcp"
#     source_address_prefix      = "*"
#     source_port_range          = "*"
#   }

#   security_rule {
#     access                     = "Allow"
#     description                = "Allow port"
#     destination_address_prefix = "*"
#     destination_port_range     = "5985"
#     direction                  = "Inbound"
#     name                       = "winrm"
#     priority                   = 300
#     protocol                   = "Tcp"
#     source_address_prefix      = "*"
#     source_port_range          = "*"
#   }

# }



# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "unix" {
  network_interface_id      = azurerm_network_interface.nic0.id
  network_security_group_id = azurerm_network_security_group.nsg_linux.id
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "win" {
  network_interface_id      = azurerm_network_interface.nic1.id
  network_security_group_id = azurerm_network_security_group.nsg_win.id
}



# Create a Linux virtual machine
resource "azurerm_virtual_machine" "vm0" {
  name                  = "${var.prefix}VM0"
  location              = var.location
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.nic0.id]
  vm_size               = "Standard_DS1_v2"
  tags                  = var.tags

  storage_os_disk {
    name              = "${var.prefix}OsDisk0"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = lookup(var.sku, var.location)
    version   = "latest"
  }

  os_profile {
    computer_name  = "${var.prefix}VM0"
    admin_username = var.admin_username
    admin_password = var.admin_password
  }

  os_profile_linux_config {
    disable_password_authentication = false
  }

}

# data "azurerm_public_ip" "ip0" {
#   name                = azurerm_public_ip.publicip0.name
#   resource_group_name = azurerm_virtual_machine.vm0.resource_group_name
#   depends_on          = ["azurerm_virtual_machine.vm0"]
# }

# Create Window VM

# data "template_file" "auto_logon" {
#   template = file("tpl.auto_logon.xml")

#   vars = {
#     admin_username = var.admin_username
#     admin_password = var.admin_password
#   }
# }


# Create a Window virtual machine
resource "azurerm_virtual_machine" "vm1" {
  name                  = "${var.prefix}VM1"
  location              = var.location
  resource_group_name   = azurerm_resource_group.rg.name
  network_interface_ids = [azurerm_network_interface.nic1.id]
  vm_size               = "Standard_DS1_v2"
  tags                  = var.tags

  storage_os_disk {
    name              = "${var.prefix}OsDisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Premium_LRS"
  }

  storage_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2016-Datacenter"
    version   = "latest"
  }

#   os_profile {
#     computer_name  = "lab1-vm1"
#     admin_username = var.admin_username
#     admin_password = var.admin_password
#     custom_data    = file("files/ConfigureRemotingForAnsible.ps1")
#   }

#   os_profile_windows_config {
#     provision_vm_agent = true
#     additional_unattend_config {
#       pass         = "oobeSystem"
#       component    = "Microsoft-Windows-Shell-Setup"
#       setting_name = "AutoLogon"
#       content      = data.template_file.auto_logon.rendered
#     }

#     additional_unattend_config {
#       pass         = "oobeSystem"
#       component    = "Microsoft-Windows-Shell-Setup"
#       setting_name = "FirstLogonCommands"
#       content      = file("tpl.first_logon_commands.xml")
#     }
#   }
# }
  
  # provisioner "local-exec" {
  #   command = "terraform output -json > ./ansible/data/ip.json; export ANSIBLE_HOST_KEY_CHECKING=False ;ansible-playbook -i ./ansible/hosts ./ansible/site.yml"
  # }

  os_profile {
    computer_name  = "${var.prefix}VM1"
    admin_username = var.admin_username
    admin_password = var.admin_password
    custom_data    = file("./files/winrm.ps1")
  }
  
  provisioner "file" {
    
    source      = "./files/config.ps1"
    destination = "c:/terraform/config.ps1"
    connection {
    type     = "winrm"
    user     = var.admin_username
    password = var.admin_password
    host     = azurerm_public_ip.publicip1.ip_address
  }
  }
  os_profile_windows_config {
    provision_vm_agent = true
    winrm {
      protocol = "http"
    }
    additional_unattend_config {
      pass          = "oobeSystem"
      component     = "Microsoft-Windows-Shell-Setup"
      setting_name  = "AutoLogon"
      content       = "<AutoLogon><Password><Value>Pa55w.rd1234</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>Student</Username></AutoLogon>"
    }
    additional_unattend_config {
      pass          = "oobeSystem"
      component     = "Microsoft-Windows-Shell-Setup"
      setting_name  = "FirstLogonCommands"
      content       = file("./files/FirstLogonCommands.xml")
    }
  }

  # connection {
  #   host    = azurerm_public_ip.publicip1.ip_address
  #   type    = "winrm"
  #   port    = 5985
  #   https   = false
  #   timeout = "10m"
  #   user    = "Student"
  #   password = "Pa55w.rd1234"
  # }


}
  # provisioner "remote-exec" {
  #   # on_failure = continue
  #   inline = [
  #     "powershell.exe -ExecutionPolicy Bypass -File C:/terraform/config.ps1",
  #   ]
  # }
  # provisioner "local-exec" {
  #   command = "terraform output -json > ./ansible/data/ip.json; export ANSIBLE_HOST_KEY_CHECKING=False ;ansible-playbook -i ./ansible/hosts ./ansible/site.yml"
  # }
# }

# data "azurerm_public_ip" "ip1" {
#   name                = azurerm_public_ip.publicip1.name
#   resource_group_name = azurerm_virtual_machine.vm1.resource_group_name
#   depends_on          = ["azurerm_virtual_machine.vm1"]
# }





output "public_ip_address_unix" {
  value = azurerm_public_ip.publicip0.ip_address
}

output "public_ip_address_win" {
  value = azurerm_public_ip.publicip1.ip_address
}
